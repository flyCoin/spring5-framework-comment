package com.shadow.mapper;

import org.apache.ibatis.annotations.Select;

public interface UserMapper {

	@Select("select * from users")
	String selectById();
}
