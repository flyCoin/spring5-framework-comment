package com.shadow.service;

import com.shadow.dao.User;
import com.shadow.mapper.OrderMapper;
import com.shadow.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class UserService {

	@Autowired
	private UserMapper userMapper; // 整合mybatis，时间上就是将一个UserMapper这个类型的对象放入到spring容器中，成为一个Bean

	@Autowired
	private OrderMapper orderMapper;
	// 这是针对一个Mapper，如果是多个mapper呢，需要添加多个mapper对象呢，不能重复性的添加类，需要写成动态的

	public void query() {
		userMapper.selectById();
		orderMapper.selectById();
	}

	// @Autowired
	// public UserService() {
	// 	System.out.println("无参");
	// }

	// @Autowired(required = false)
	// public UserService(User user) {
	// 	System.out.println("一个");
	// }

	// @Autowired(required = false)
	// public UserService(User user, User user1) {
	// 	System.out.println("二个");
	// }
}
