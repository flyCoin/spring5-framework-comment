package com.shadow.dao;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
public class Person implements ApplicationContextAware {

	@Autowired
	private User user123;

	// public void setUser(User user) {
	// 	this.user = user;
	// }

	public void test() {
		System.out.println(user123);
	}

	@Override
	@Autowired
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println(111);
	}
}
