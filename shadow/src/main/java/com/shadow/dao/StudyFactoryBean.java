package com.shadow.dao;

import com.shadow.mapper.UserMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class StudyFactoryBean implements FactoryBean {

	Class mapperInterface;

	public StudyFactoryBean(Class mapperInterface) {
		this.mapperInterface = mapperInterface;
	}

	@Override
	public Object getObject() throws Exception {
		Object proxyInstance = Proxy.newProxyInstance(StudyFactoryBean.class.getClassLoader(), new Class[]{mapperInterface}, new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

				// 执行代理的逻辑，执行sql语句
				// Select annotation = method.getAnnotation(Select.class);
				System.out.println(method.getName());
				// String[] value = annotation.value();
				// System.out.println(value[0]);
				return null;
			}
		});

		return proxyInstance;
	}

	@Override
	public Class<?> getObjectType() {
		return mapperInterface;
	}
}
