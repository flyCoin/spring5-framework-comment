package com.shadow.test;

import com.shadow.beanFactoryPostProcessor.ShadowBeanNameGenerator;
import com.shadow.beanFactoryPostProcessor.TestBeanFactoryPostProcessor;
import com.shadow.beanFactoryPostProcessor.TestXXXBeanFactoryPostProcessor;
import com.shadow.config.Config;
import com.shadow.dao.Person;
import com.shadow.dao.User;
import com.shadow.dao.UserDao;
import com.shadow.service.UserService;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.annotation.AnnotatedBeanDefinitionReader;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.math.BigInteger;

public class Test {

	// 网BeanFactory中添加BeanDefinition的方法
	// 1. 使用@ComponentScan注解，BeanFactory通过代码扫描到添加了@Component注解的类，将他们添加到BeanDefinitionMap中
	// 2. 使用@Import注解，添加一个没有在此类上添加@Component注解的类为BeanDefinition，也可以添加实现了ImportBeanDefinitionRegistrar的类，
	//    Spring在启动的时候，会调用这个这个实现类的方法，在这个实现类的方法中，可以去注册BeanDefinition
	// 3. 可以是一个实现了BeanDefinitionRegistryPostProcessor接口的类，Spring在启动的时候，也会调用这个实现类的方法，在这个实现类的方法中，
	//    可以将BeanDefinition注册到BeanFactory中去
	public static void main(String[] args) {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(Config.class);
		UserService bean = applicationContext.getBean(UserService.class);
		bean.query();
		// AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		// applicationContext.register(Config.class);
		// applicationContext.getBeanFactory().registerSingleton(AnnotationConfigUtils.CONFIGURATION_BEAN_NAME_GENERATOR,
		// 		new ShadowBeanNameGenerator());
		// applicationContext.getEnvironment().setRequiredProperties("shadowXXX");
		// applicationContext.refresh();
		// User bean = applicationContext.getBean(User.class);
		// System.out.println(bean);
		// ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");
		// System.out.println(annotationConfigApplicationContext.getBean("user"));
		// Person person = applicationContext.getBean("person", Person.class);
		// person.test();
		// System.out.println(annotationConfigApplicationContext.getBean("studyFactoryBean"));

		// ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(annotationConfigApplicationContext);
		// scanner.scan("com.shadow.dao");

		// 通过注解的方式将一个类设置为bean
		// AnnotatedBeanDefinitionReader annotatedBeanDefinitionReader = new AnnotatedBeanDefinitionReader(annotationConfigApplicationContext);
		// 将Person.class解析为BeanDefinition
		// annotatedBeanDefinitionReader.register(Person.class);
		// System.out.println(annotationConfigApplicationContext.getBean("person"));

		// 使用xmlBeanDefinitionReader去读取定义bean的xml标签，生成bean
		// XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(annotationConfigApplicationContext);
		// int i = xmlBeanDefinitionReader.loadBeanDefinitions("spring.xml");
		// System.out.println(i);
		// System.out.println(annotationConfigApplicationContext.getBean(Person.class));


		// 设置成一个beanDefinition
		// AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
		// beanDefinition.setBeanClass(Person.class);

		// 向容器中注册beanDefinition
		// annotationConfigApplicationContext.registerBeanDefinition("person", beanDefinition);
		// System.out.println(annotationConfigApplicationContext.getBean(Person.class));

		// UserDao dao = annotationConfigApplicationContext.getBean(UserDao.class);
		// dao.test();
		// UserDao dao1 = annotationConfigApplicationContext.getBean(UserDao.class);
		// System.out.println(dao.hashCode() + "=========" + dao1.hashCode());
		// System.out.println(dao.hashCode());
	}
}
