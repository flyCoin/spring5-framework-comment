package com.shadow.config;

import com.shadow.dao.StudyBeanDefinitionRegister;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@ComponentScan("com.shadow")
// @MapperScan
// @Import(StudyBeanDefinitionRegister.class)
@EnableAspectJAutoProxy
public class Config {

	// @Bean
	// public User user1() {
	// 	return new User();
	// }

	// @Bean
	// public DataSource dataSource() {
	// 	DriverManagerDataSource dataSource = new DriverManagerDataSource();
	// 	dataSource.setUrl("jdbc:mysql://localhost:3306/general");
	// 	dataSource.setUsername("root");
	// 	dataSource.setPassword("root");
	// 	return dataSource;
	// }

	// @Bean
	// public SqlSessionFactoryBean sqlSessionFactoryBean() {
	// 	SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
	// 	sqlSessionFactoryBean.setDataSource(dataSource());
	// 	return sqlSessionFactoryBean;
	// }
}
